import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import logo from './logo.svg';
import './App.css';
import { Glyphicon } from "react-bootstrap";
import { connect } from "react-redux";
import userAction from "./store/actions/userAction"
import shoppingCartAction from "./store/actions/shoppingCartAction"

import Home from "./scenes/Home"
import Cart from "./scenes/ShoppingCart"
import Account from "./scenes/Account"

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Switch>
          <Route path="/" component={Home} exact/>
          <Route path="/shoppingCart" component={Cart} />
          <Route path="/account" component={Account} />       
          </Switch> 
        </div>
      </BrowserRouter>
      )
  }
}

export default App