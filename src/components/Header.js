import React from "react";
import { Link } from "react-router-dom";

export default class Header extends React.Component {
	render() {
		return (
			<header>
				<nav>
					<Link to="/">Home |</Link>
					<Link to="shoppingcart"> Shopping Cart |</Link>
					<Link to="account"> Account</Link>
				</nav>
				<h1>This is simple redux shop</h1>
			</header>
		);
	}
}