import { applyMiddleware, combineReducers, createStore } from "redux";
import reducer from './reducers/reducer';
import thunk from "redux-thunk";
import logger from "redux-logger";

const middleware = applyMiddleware(thunk, logger);
const store = createStore(reducer, undefined, middleware)

export default store
