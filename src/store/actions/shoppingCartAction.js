import shopApi from "../../api/shopApi"
export default {
	addProduct: (product) => {
		return {
			type: "ADD_PRODUCT",
			payload: product
		}
	},

	updateProduct: (id, number) => {
		return {
			type: "UPDATE_PRODUCT",
			payload: {
				id,
				number
			}
		}
	},

	deleteProduct: (id) => {
		return {
			type: "DELETE_PRODUCT",
			payload: {
				id
			}
		}
	},

	deleteAllProduct: () => {
		return {
			type: "CLEAR_CART",
		}
	}
}
