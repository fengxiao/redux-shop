export default {
	fetchUser: () => {
		return {
			type: "FETCH_USER_FULFILLED",
			payload: {
				name: "Dick",
				age: 35,
			}
		}
	},
	setUserName: (name) => {
		return {
			type: "SET_USER_NAME",
			payload: {
				name: name,
			}
		}
	},
	setUserAge: (age) => {
		return {
			type: "SET_USER_AGE",
			payload: {
				age: age,
			}
		}
	},
}






