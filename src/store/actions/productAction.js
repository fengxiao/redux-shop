import shopApi from "../../api/shopApi"
export default {
	getProducts: () => {
		return (dispatch) => {
	      	shopApi
	        	.customerFilter({
		            page: 1,
		            keyword: "",
		            sub: [],
		            district: [],
		            categoryId: 1,
		            orderBy: "score",
		            order: "desc"
	          	})
	          	.then( response => {
	            	const shops = response.data.shops;
	            	dispatch({type: "FETCH_PRODUCTS_FULFILLED", payload:shops})
	          	})
	          	.catch((err)=>{
	          		console.log(err)
	          		dispatch({type: "FETCH_PRODUCTS_ERROR", payload:err})
	          	})
			} 
	},
}
