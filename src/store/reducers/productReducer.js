const initialState = {
	fetched: false,
	products: [],
}
export default (state=initialState, actions) => {
	switch(actions.type){
		case 'FETCH_PRODUCTS_STARTED':
			break		
		case 'FETCH_PRODUCTS_FULFILLED':
			state = {...state}
			state.fetched = true
			state.products = state.products.concat(actions.payload)
			break
	}
	return state	
}