import { combineReducers } from 'redux';
import userReducer from './userReducer';
import productReducer from './productReducer';
import shoppingCartReducer from './shoppingCartReducer';


export default combineReducers({
  user: userReducer,
  products: productReducer,
  cartProducts: shoppingCartReducer,
});