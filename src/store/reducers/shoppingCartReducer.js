export default (state=[], actions) => {
	switch(actions.type){
		case "ADD_PRODUCT":
			const index1 = state.findIndex(item=>item.id==actions.payload.id)
			if(index1!=-1){
				state = state.map(item =>{return {...item}})
				state[index1].number = parseInt(state[index1].number) + parseInt(actions.payload.number)
			} else
				state = state.concat([{...actions.payload}])
			break
		case "UPDATE_PRODUCT":
			const index2 = state.findIndex(item=>item.id==actions.payload.id)
			state = state.map(item=>{return {...item}})
			state[index2].number = actions.payload.number
			break
		case "DELETE_PRODUCT":
			state = state.map(item=>{return {...item}})
			state = state.filter(item=>item.id!=actions.payload.id)
			break
		case "CLEAR_CART":
			state = []
			break
	}
	return state	
}