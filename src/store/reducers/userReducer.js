const initialState = {
	fetched: false,
	user: {
		id: null,
		name: null,
		age: null,
	}
}
export default (state=initialState, actions) => {
	switch(actions.type){
		case "FETCH_USER_FULFILLED":
			state.fetched = true
			state.user = {...state.user, ...actions.payload}
			break
		case "SET_USER_NAME":
			state = {...state, name:actions.payload.name}
			break
		case "SET_USER_AGE":
			state = {...state, age:actions.payload.age}		
			break
	}
	return state		
}