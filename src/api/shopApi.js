const CONFIG = {    
	API_ROOT:"https://api.localrecommends.com/api/",
    IMAGE_HOST:"https://s3-ap-southeast-1.amazonaws.com/data.localrecommends.com/prod/"
	}

const FetchWithoutHeaders = async function (url, data) {
    let request = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    };
    
    if (data) {
        request.body = JSON.stringify(data);
    }

    return fetch(url, request)
        .then((response)=>{
            return response.json();
        }, (error)=>{
            return error
        });
}

export default {
    customerFilter: (params) => {
        return FetchWithoutHeaders(CONFIG.API_ROOT + 'shop/customerFilter', params)
    },
}



