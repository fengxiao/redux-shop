import React, { Component } from 'react';

import '../App.css';
import { Glyphicon } from "react-bootstrap";
import { connect } from "react-redux";
import userAction from "../store/actions/userAction"
import shoppingCartAction from "../store/actions/shoppingCartAction"

import Header from "../components/Header"

class Account extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <div style={{width:1200, margin:'100px auto', textAlign:'center',}}>
          This is your account.         
        </div>       
      </div>
    );
  }
}

export default Account;