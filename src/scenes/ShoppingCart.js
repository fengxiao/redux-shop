import React, { Component } from 'react';
import '../App.css';
import { Glyphicon } from "react-bootstrap";
import { connect } from "react-redux";
import userAction from "../store/actions/userAction"
import shoppingCartAction from "../store/actions/shoppingCartAction"

import Header from "../components/Header"

class Cart extends Component {
  cascadeNames = (array_to_sort, byName) => {
    if (!array_to_sort) return;
    let new_array = array_to_sort.slice();
    // console.log(new_array);
    if (byName) {
      let sorted = new_array.sort(function(a, b) {
        return a.name.localeCompare(b.name);
      });
      return sorted.map(item => item.name).join(", ");
    } else {
      let sorted = new_array.sort(function(a, b) {
        return a.localeCompare(b);
      });
      return sorted.join(", ");
    }
  }  
  onQuantityChange = (e, shop) => {
    this.props.dispatch(shoppingCartAction.updateProduct(shop.id, e.target.value))
  }
  deleteProduct = (id) => {
    this.props.dispatch(shoppingCartAction.deleteProduct(id)) 
  }

  render() {
    return (
      <div className="App">
        <Header/>
        <div style={{width:1200, margin:'100px auto', textAlign:'center',}}>
          <div style={{margin:40}}>Your current shopping list.</div>
          <span style={{width:800, display:'inline-block', textAlign:'left'}}>
            <span style={{width:580, display:'inline-block', textAlign:'center',}}>Item</span>
            <span style={{width:60, display:'inline-block', textAlign:'center',}}>Quantity</span>
            <span style={{width:80, display:'inline-block', textAlign:'center',}}>Price</span>            
          </span>
          {
            this.props.cartProducts.map((shop, index) => (
              <div className="shop-tab" key={index} style={{
                width:780,
                margin:'20px auto',
                padding:10,
                background:'#eee',
                display:'table',
                }}>
                <a href={'https://www.localrecommends.com/shop/' + shop.nameid} style={{
                  display:'inline-block',
                  height:140,
                  width:210,
                  overflow:'hidden',
                }}> 
                    <img src={'https://s3-ap-southeast-1.amazonaws.com/data.localrecommends.com/prod/'+ shop.album[0].uuid + '_thumb'} 
                    style={{height: 140, width: 210, objectFit: 'cover', borderRadius:4}} />            
                </a>
                <div class="shop-info" style={{display:'inline-block',width:320, marginLeft:10, textAlign:'left', verticalAlign: 'top'}}>
                  <a className="list-shop-name open-sans" href={'https://www.localrecommends.com/shop/' + shop.nameid} style={{display:'inline-block', fontSize:16, fontWeight:600, color:'black', textDecoration:'None'}}>
                    { index + 1 }.{' '}{ shop.name }
                  </a>           

                  <div style={{lineHeight:'30px',fontSize:14}}>
                    <Glyphicon glyph="star" />
                  </div>                
                  <div style={{lineHeight:'30px',fontSize:14}}>
                    <span class="shop-subcategories">{this.cascadeNames(shop.subCategories,true)}</span>  
                  </div>
                  <div style={{lineHeight:'30px',fontSize:14}}>
                    <span class="shop-subcategories">{this.cascadeNames(shop.district, false)}</span>  
                  </div>
                  <div style={{lineHeight:'30px',fontSize:14}}>{shop.address}</div>    
                </div>       
                <span style={{display:'table-cell', verticalAlign:'middle', width:60}}>
                  <input type='number' min="1" defaultValue ='1' 
                    style={{width:30}} 
                    value={shop.number} 
                    onChange={(event)=>this.onQuantityChange(event,shop)}/>
                </span>
                <span style={{display:'table-cell', verticalAlign:'middle', width:60}}>${(shop.number*parseInt(shop.nameid.replace(/\D/g,''))/100).toFixed(1)}</span>
                <span style={{display:'table-cell', verticalAlign:'middle', width:80}}>
                  <button onClick={()=>this.deleteProduct(shop.id)}>Delete</button>
                </span>
                
              </div>
              ))
          } 
          { this.props.cartProducts.length?
            <div style={{width:1200, lineHeight:'200px',textAlign:'center', whiteSpace:'pre-wrap'}}>{'Total:    $' + this.props.cartProducts.reduce((accumulator, item)=>
              accumulator + (item.number*parseInt(item.nameid.replace(/\D/g,''))/100),0).toFixed(1)}</div>:
            <div style={{width:1200, lineHeight:'200px',textAlign:'center'}}>Your shopping list is empty</div>

          }                     
          <button>Checkout</button>
        </div>       
      </div>
    );
  }
}

Cart = connect((store)=>{
  return {
    cartProducts: store.cartProducts
  }
})(Cart)

export default Cart;