import React, { Component } from 'react';

import '../App.css';
import { Glyphicon } from "react-bootstrap";
import { connect } from "react-redux";

import userAction from "../store/actions/userAction"
import productAction from "../store/actions/productAction"
import shoppingCartAction from "../store/actions/shoppingCartAction"

import Header from "../components/Header"

class Home extends Component {
  componentDidMount = () => {
    if(!this.props.user.fetched)
      this.props.dispatch(userAction.fetchUser())
    if(!this.props.products.fetched)
      this.props.dispatch(productAction.getProducts())
  }

  addToShoppingCart = (shop) => {
    console.log(shop.number)
    this.props.dispatch(shoppingCartAction.addProduct(shop));
  }

  onQuantityChange = (e, shop) => {
    shop.number = e.target.value
  }

  cascadeNames = (array_to_sort, byName) => {
    if (!array_to_sort) return;
    let new_array = array_to_sort.slice();
    if (byName) {
      let sorted = new_array.sort(function(a, b) {
        return a.name.localeCompare(b.name);
      });
      return sorted.map(item => item.name).join(", ");
    } else {
      let sorted = new_array.sort(function(a, b) {
        return a.localeCompare(b);
      });
      return sorted.join(", ");
    }
  }  

  render() {
    return (
      <div className="App">
        <Header/>
        <div style={{width:1200, margin:'100px auto', textAlign:'left',}}>
          {
            this.props.products.products.map((shop, index) => {
              shop.number = 1;
              return (
              <div key={index} style={{
                width:540,
                margin:20,
                padding:10,
                background:'#eee',
                display:'inline-block',
                }}>
                <a href={'https://www.localrecommends.com/shop/' + shop.nameid} style={{
                  display:'inline-block',
                  height:140,
                  width:210,
                  overflow:'hidden',
                }}> 
                    <img src={'https://s3-ap-southeast-1.amazonaws.com/data.localrecommends.com/prod/'+ shop.album[0].uuid + '_thumb'} 
                    style={{height: 140, width: 210, objectFit: 'cover', borderRadius:4}} />            
                </a>
                <div style={{display:'inline-block',width:320, marginLeft:10, textAlign:'left', verticalAlign: 'top'}}>
                  <a href={'https://www.localrecommends.com/shop/' + shop.nameid} style={{display:'inline-block', fontSize:16, fontWeight:600, color:'black', textDecoration:'None'}}>
                    { index + 1 }.{' '}{ shop.name }
                  </a>           

                  <div style={{lineHeight:'30px',fontSize:14}}>
                    <Glyphicon glyph="star" />
                  </div>                
                  <div style={{lineHeight:'30px',fontSize:14}}>
                    <span>{this.cascadeNames(shop.subCategories,true)}</span>  
                  </div>
                  <div style={{lineHeight:'30px',fontSize:14}}>{shop.address}</div>    
                  <div style={{lineHeight:'30px',fontSize:14, color:'#0abab5'}}>Price: ${(parseInt(shop.nameid.replace(/\D/g,''))/100).toFixed(1)}{/* Generate a fake price based on shop.nameid */}</div>                  
                </div>   
                <div style={{lineHeight:'30px'}}>
                  <span style={{verticalAlign:'middle'}}>Quantity: </span>
                  <input type='number' min="1" defaultValue ='1' style={{width:40}} onChange={(event)=>this.onQuantityChange(event,shop)}></input>
                  <button style={{float:'right'}} onClick={()=>this.addToShoppingCart(shop)}>Add to Shopping Cart</button>                 
                </div>
              </div>
              )              
            })
          }             
        </div>       
      </div>
    );
  }
}

Home = connect((store)=>{
  return {
    user: store.user,
    products: store.products
  }
})(Home)

export default Home;