import React, { Component } from 'react';
import './App.css';
import { Glyphicon } from "react-bootstrap";
import { connect } from "react-redux";
import userAction from "./store/actions/userAction"
import shoppingCartAction from "./store/actions/shoppingCartAction"

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Switch>
          <Route path="/" component={Home} exact/>
          <Route path="/shoppingCart" component={Cart} />
          <Route path="/account" component={AccountPage} />          
          <Route component={ErrorPage} />  
          </Switch> 
        </div>
      </BrowserRouter>
      )
  }
}
class App extends Component {
  componentDidMount = () => {
    this.props.dispatch(userAction.fetchUser())
    this.props.dispatch(shoppingCartAction.getProducts())
  }
  changeName = () => {
    this.props.dispatch(userAction.setUserName('Johnny'))
  }
  changeAge = () => {
    this.props.dispatch(userAction.setUserAge(36))
  }  

  cascadeNames = (array_to_sort, byName) => {
    if (!array_to_sort) return;
    let new_array = array_to_sort.slice();
    // console.log(new_array);
    if (byName) {
      let sorted = new_array.sort(function(a, b) {
        return a.name.localeCompare(b.name);
      });
      return sorted.map(item => item.name).join(", ");
    } else {
      let sorted = new_array.sort(function(a, b) {
        return a.localeCompare(b);
      });
      return sorted.join(", ");
    }
  }  
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React, {this.props.user.name} who is {this.props.user.age}</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <div>
          <button onClick={this.changeName}>Click to change my name</button>
          <button onClick={this.changeAge}>Click to change my age</button>        
        </div>
        <div style={{width:1200, margin:'100px auto', display:'flex', flexWrap:'wrap'}}>
          {
            this.props.cartProducts.map((shop, index) => (
              <div className="shop-tab" key={index} style={{minHeight:200, 
                width:538,
                margin:20,
                padding:10,
                background:'#eee',
                display:'flex',
                }}>
                <a href={'https://www.localrecommends.com/shop/' + shop.nameid} style={{
                  display:'inline-block',
                  height:140,
                  width:210,
                  overflow:'hidden',
                }}> 
                    <img src={'https://s3-ap-southeast-1.amazonaws.com/data.localrecommends.com/prod/'+ shop.album[0].uuid + '_thumb'} 
                    style={{height: 140, width: 210, objectFit: 'cover', borderRadius:4}} />            
                </a>
                <div class="shop-info" style={{display:'inline-block',width:318, marginLeft:10, textAlign:'left'}}>
                  <a className="list-shop-name open-sans" href={'https://www.localrecommends.com/shop/' + shop.nameid} style={{display:'inline-block', fontSize:16, fontWeight:600, color:'black', textDecoration:'None'}}>
                    { index + 1 }.{' '}{ shop.name }
                  </a>           

                  <div style={{lineHeight:'30px',fontSize:14}}>
                    <Glyphicon glyph="star" />
                  </div>                
                  <div style={{lineHeight:'30px',fontSize:14}}>
                    <span class="shop-subcategories">{this.cascadeNames(shop.subCategories,true)}</span>  
                  </div>
                  <div style={{lineHeight:'30px',fontSize:14}}>
                    <span class="shop-subcategories">{this.cascadeNames(shop.district, false)}</span>  
                  </div>
                  <div style={{lineHeight:'30px',fontSize:14}}>{shop.address}</div>    
                </div>                    
              </div>
              ))
          }             
        </div>       
      </div>
    );
  }
}

App = connect((store)=>{
  return {
    user: store.user,
    cartProducts: store.cartProducts
  }
})(App)

export default App;
